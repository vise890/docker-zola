FROM debian:stable-slim

LABEL description="Docker image for running the Zola SSG with GitLab pipelines."
LABEL maintainer="Martino Visintin <martino@visint.in>"

RUN apt-get update \
  && apt-get install --yes curl \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

ENV ZOLA_VERSION=v0.13.0

RUN curl -L https://github.com/getzola/zola/releases/download/$ZOLA_VERSION/zola-$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz > zola.tar.gz \
  && tar xvf zola.tar.gz \
  && rm -rf zola.tar.gz \
  && mv zola /usr/bin/

ENTRYPOINT ["/usr/bin/zola"]
